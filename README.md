## JAVA Web开发技术应用——监听器
>简介：本课程从Java Web中的监听器的概念和用途入手，紧接着讲解监听器的第一个实例，然后由浅到深地从启动顺序到分类、从Servelt2.5到 Servlet3.0，重点讲解了两种分类，包括按监听的对象划分和按监听的事件划分，最后以一个实际项目中统计在线人数的案例进行讲解。
### 第1章 监听器简介
>介绍监听器的概念和用途。
+ 1-1 概述 (06:34)
+ 1-2 Web监听器的用途 (01:14)
### 第2章 监听器的实现及启动顺序
>介绍了第一个监听器的案例，以及监听器的启动顺序。
 2-1 第一个监听器案例 (03:35)
 2-2 监听器的启动顺序 (00:52)
### 第3章 监听器的分类
>介绍用于监听ServletContext、HttpSession和ServletRequest的事件监听器和按监听的事件划分的监听器及实现。
+ 3-1 ServletContext的事件监听器 (04:49)
+ 3-2 HttpSession的事件监听器 (06:51)
+ 3-3 ServletRequest的事件监听器 (04:19)
+ 3-4 属性的增加和删除的事件监听器 (15:04)
+ 3-5 绑定到HttpSession域中的对象状态的事件监听器 (15:25)
### 第4章 Servlet 3.0下监听器的使用
>介绍在Servlet 3.0下，监听器是如何使用的。
+ 4-1 Servlet3.0下监听器的使用 (04:59)
### 第5章 项目案例
>介绍统计在线用户及人数功能的实现，以及来进一步地巩固监听器的知识。
+ 5-1 统计在线用户及人数 (18:58)
### 第6章 总结
>总结一下所学的监听器的内容及需重点注意的问题。
+ 6-1 监听器总结 (02:45)